package setting

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"goAdapter/utils"
	"net"
	"os/exec"
	"runtime"
	"strings"
)

type NetworkNameListTemplate struct {
	Name []string `json:"Name"`
}

type NetworkParamTemplate struct {
	Index       int                        `json:"Index"` // positive integer that starts at one, zero is never used
	MTU         int                        `json:"MTU"`   // maximum transmission unit
	Name        string                     `json:"Name"`  // e.g., "en0", "lo0", "eth0.100"
	MAC         string                     `json:"MAC"`   // IEEE MAC-48, EUI-48 and EUI-64 form
	Flags       string                     `json:"Flags"` // e.g., FlagUp, FlagLoopback, FlagMulticast
	IP          string                     `json:"IP"`
	Netmask     string                     `json:"Netmask"`
	Gateway     string                     `json:"Gateway"`
	ConfigParam NetworkConfigParamTemplate `json:"ConfigParam"`
}

type NetworkConfigParamTemplate struct {
	Name          string `json:"Name"`
	ConfigEnable  bool   `json:"ConfigEnable"`
	DHCPEnable    bool   `json:"DHCPEnable"`
	ConfigIP      string `json:"ConfigIP"`
	ConfigNetmask string `json:"ConfigNetmask"`
	ConfigGateway string `json:"ConfigGateway"`
}

type NetworkParamMapTemplate struct {
	NetworkParam map[string]NetworkParamTemplate `json:"NetworkParam"`
}

var NetworkParamMap = &NetworkParamMapTemplate{
	NetworkParam: make(map[string]NetworkParamTemplate, 0),
}

var NetworkConfigParamList = make(map[string]NetworkConfigParamTemplate, 0)

func init() {

}

//获取当前网络参数
func GetNetworkParam() {
	inters, err := net.Interfaces()
	if err != nil {
		ZAPS.Errorf("获取网卡信息错误 %v", err)
		return
	}

	param := NetworkParamTemplate{}
	for _, v := range inters {
		param.Index = v.Index
		param.Name = v.Name
		param.MTU = v.MTU
		param.MAC = strings.ToUpper(hex.EncodeToString(v.HardwareAddr))
		param.Flags = v.Flags.String()
		param.IP, param.Netmask = GetIPAndMask(v.Name)
		param.Gateway = GetGateway(v.Name)
		param.ConfigParam = NetworkConfigParamList[v.Name]

		NetworkParamMap.NetworkParam[v.Name] = param

	}
}

func GetIPAndMask(name string) (ip, netmask string) {

	inter, err := net.InterfaceByName(name)
	if err != nil {
		return "", ""
	}
	address, _ := inter.Addrs()
	for _, addr := range address {
		if ip, ok := addr.(*net.IPNet); ok && !ip.IP.IsLoopback() {
			if ip.IP.To4() != nil {
				return ip.IP.String(), net.IP(ip.Mask).String()
			}
		}
	}

	return "", ""
}

func GetGateway(name string) string {
	if runtime.GOOS == "linux" {
		out, err := exec.Command("/bin/sh", "-c",
			fmt.Sprintf("route -n | grep %s | grep UG | awk '{print $2}'", name)).Output()
		if err != nil {
			return ""
		}
		return strings.Trim(string(out), "\n")
	}

	return ""
}

func AddNetworkConfigParam(configParam NetworkConfigParamTemplate) error {

	param, ok := NetworkParamMap.NetworkParam[configParam.Name]
	if ok == false {
		return errors.New("网卡名字不存在")
	}

	NetworkConfigParamList[configParam.Name] = configParam
	param.ConfigParam = configParam
	NetworkParamMap.NetworkParam[configParam.Name] = param
	NetworkParaWrite()

	return nil
}

//修改网络参数
func ModifyNetworkConfigParam(configParam NetworkConfigParamTemplate) error {

	param, ok := NetworkParamMap.NetworkParam[configParam.Name]
	if ok == false {
		return errors.New("网卡名字不存在")
	}

	NetworkConfigParamList[configParam.Name] = configParam
	param.ConfigParam = configParam
	NetworkParamMap.NetworkParam[configParam.Name] = param
	NetworkParaWrite()

	return nil
}

//删除网络参数
func DeleteNetworkConfigParam(name string) error {
	param, ok := NetworkParamMap.NetworkParam[name]
	if ok == false {
		return errors.New("网卡名字不存在")
	}

	delete(NetworkConfigParamList, name)
	NetworkParaWrite()

	configParam := NetworkConfigParamTemplate{}
	param.ConfigParam = configParam
	NetworkParamMap.NetworkParam[name] = param

	return nil
}

func (n *NetworkParamTemplate) CmdSetDHCP() error {

	//非阻塞,动态获取IP有可能不成功
	out, err := exec.Command("/bin/sh", "-c",
		fmt.Sprintf("udhcpc -i %s", n.Name)).Output()
	if err != nil {
		ZAPS.Debugf("网卡[%s]动态获取IP失败 %s %v", n.Name, string(out), err)
		return err
	}
	ZAPS.Debugf("网卡[%s]动态获取IP成功 %s", n.Name, string(out))

	return nil
}

func (n *NetworkParamTemplate) CmdSetStaticIP() {

	//strNetMask := "netmask " + n.Netmask
	//cmd := exec.Command("ifconfig",
	//	n.Name,
	//	n.IP,
	//	strNetMask)
	//
	//var out bytes.Buffer
	//cmd.Stdout = &out
	//cmd.Start() //执行到此,直接往后执行

	out, err := exec.Command("/bin/sh", "-c",
		fmt.Sprintf("ifconfig %s %s netmask %s", n.Name, n.ConfigParam.ConfigIP, n.ConfigParam.ConfigNetmask)).Output()
	if err != nil {
		ZAPS.Debugf("网卡[%s]设置IP[%s]Netmask[%s]失败 %s %v", n.Name, n.ConfigParam.ConfigIP, n.ConfigParam.ConfigNetmask, string(out), err)
	} else {
		ZAPS.Debugf("网卡[%s]设置IP[%s]Netmask[%s]成功", n.Name, n.ConfigParam.ConfigIP, n.ConfigParam.ConfigNetmask)
	}

	out, err = exec.Command("/sbin/route", "add", "default", "gw", n.ConfigParam.ConfigGateway).Output()
	if err != nil {
		ZAPS.Debugf("网卡[%s]添加默认网关[%s]失败 %s %v", n.Name, n.ConfigParam.ConfigGateway, string(out), err)
		return
	}
	ZAPS.Debugf("网卡[%s]添加默认网关[%s]成功", n.Name, n.ConfigParam.ConfigGateway)

}

func NetworkParaRead() bool {
	data, err := utils.FileRead("/selfpara/networkPara.json")
	if err != nil {
		if strings.Contains(err.Error(), "no such directory") {
			ZAPS.Debug("打开网络配置json文件失败")
		} else {
			ZAPS.Debugf("打开网络配置json文件失败 %v", err)
		}
		return false
	}

	err = json.Unmarshal(data, &NetworkConfigParamList)
	if err != nil {
		ZAPS.Errorf("格式化网络配置json文件失败 %v", err)
		return false
	}
	ZAPS.Debug("打开网络配置json文件成功")

	GetNetworkParam()
	for _, v := range NetworkParamMap.NetworkParam {
		if v.ConfigParam.ConfigEnable == false {
			continue
		}
		if v.ConfigParam.DHCPEnable == true {
			_ = v.CmdSetDHCP()
		} else {
			v.CmdSetStaticIP()
		}
	}

	return true
}

func NetworkParaWrite() {
	utils.DirIsExist("./selfpara")
	sJson, _ := json.Marshal(NetworkConfigParamList)

	err := utils.FileWrite("/selfpara/networkPara.json", sJson)
	if err != nil {
		ZAPS.Warnf("写入网络配置json文件失败 %v", err)
		return
	}
	ZAPS.Debugf("写入网络配置json文件成功")
}
