package commInterface

import (
	"os"
)

type CommunicationInterface interface {
	Open() bool
	Close() bool
	WriteData(data []byte) int
	ReadData(data []byte) int
	GetName() string
	GetTimeOut() string
	GetInterval() string
	GetType() int
}

const (
	CommTypeIoIn      int = 0
	CommTypeIoOut     int = 1
	CommTypeSerial    int = 2
	CommTypeTcpClient int = 3
	CommTypeTcpServer int = 4
)

//通信接口Map
var CommunicationInterfaceMap = make([]CommunicationInterface, 0)

func fileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}

func CommInterfaceInit() {

	//获取串口通信接口参数
	if ReadCommSerialInterfaceListFromJson() == true {
		for _, v := range CommunicationSerialMap {
			CommunicationInterfaceMap = append(CommunicationInterfaceMap, v)
		}
	}

	//获取TcpClient通信接口参数
	if ReadCommTcpClientInterfaceListFromJson() == true {
		for _, v := range CommunicationTcpClientMap {
			CommunicationInterfaceMap = append(CommunicationInterfaceMap, v)
		}
	}

	//获取TcpServer通信接口参数
	if ReadCommTcpServerInterfaceListFromJson() == true {
		for _, v := range CommunicationTcpServerMap {
			CommunicationInterfaceMap = append(CommunicationInterfaceMap, v)
		}
	}

	//获取开关量输出通信接口参数
	if ReadCommIoOutInterfaceListFromJson() == true {
		for _, v := range CommunicationIoOutMap {
			CommunicationInterfaceMap = append(CommunicationInterfaceMap, v)
		}
	}

	//获取开关量输入通信接口参数
	if ReadCommIoInInterfaceListFromJson() == true {
		for _, v := range CommunicationIoInMap {
			CommunicationInterfaceMap = append(CommunicationInterfaceMap, v)
		}
	}

	for _, v := range CommunicationInterfaceMap {
		v.Open()
	}
}
